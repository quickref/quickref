## Version 4.0b1
- Improve the parallel support.
- Upgrade build environment to Debian Bullseye.
- Follow changes in Declt 4.0b1 and b2.

# Version 3.0 "The Alchemist"
- Follow changes in Declt 3.

# Version 2.0 "Be Quick or Be Dead"
- New index referencing libraries per author(s).
- Support for parallel (multi-threaded) builds.

# Version 1.0 "The Trooper"
- First public release.
