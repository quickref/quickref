;;; cmark.lisp --- Entry points

;; Copyright (C) 2018 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(defpackage :fr.epita.lrde.quickref.cmark
  (:documentation "The Quickref/CMark package.")
  (:use :cl)
  (:export :cmark-markdown-to-html))

(in-package :fr.epita.lrde.quickref.cmark)

(cffi:define-foreign-library libcmark
  (:darwin "libcmark.dylib")
  (:unix "libcmark.so")
  (t (:default "libcmark")))

(cffi:use-foreign-library libcmark)

(cffi:defcfun "cmark_markdown_to_html" :string
  (md :string)
  (size :unsigned-int)
  (options :int))

;;; cmark.lisp ends here
