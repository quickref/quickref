;;; fr.epita.lrde.quickref.cmark.asd --- Quickref CMark ASDF system definition

;; Copyright (C) 2018, 2022 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:load-system :fr.epita.lrde.quickref.setup)

(asdf:defsystem :fr.epita.lrde.quickref.cmark
  :long-name "Quick Lisp Reference Manuals, CMark library"
  :description "Quickref's CMark bindings"
  :long-description "\
The Quickref CMark library provides bindings to libcmark, the C implementation
of CommonMark. CommonMark[1] is itself an attempt at properly specifying the
MarkDown[2] format. For a more complete description of Quickref, see the
`fr.epita.lrde.quickref' system.

[1] http://commonmark.org/
[2] https://daringfireball.net/projects/markdown/"
  :author "Antoine Martin"
  :maintainer ("Didier Verna" "Antoine Martin")
  :mailto "quickref-devel@common-lisp.net"
  :homepage
  "http://www.lrde.epita.fr/~didier/software/lisp/typesetting.php#quickref"
  :source-control "https://gitlab.common-lisp.net/quickref/quickref"
  :license "BSD"
  :version #.(fr.epita.lrde.quickref.setup:version :short)
  :depends-on (:fr.epita.lrde.quickref.setup :cffi)
  :serial t
  :components ((:file "cmark")))

;;; fr.epita.lrde.quickref.cmark.asd ends here
