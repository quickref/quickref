set terminal svg
set output "definitions.svg"

set style fill solid 0.5
set boxwidth .8 absolute

set xlabel "Programmatic definitions"
unset xtics
set ylabel "Definitions number"

plot "definitions.dat" \
     using 2 with boxes lc rgb"green" title "Definition numbers", \
     "" using 0:2:1 with labels rotate by 75 left offset char 0,.5 notitle
