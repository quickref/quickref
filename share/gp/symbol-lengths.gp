set terminal svg
set output 'symbol-lengths.svg'

set style fill solid 0.5
set xlabel "Symbol length"
set ylabel "Symbols number"
set xtics 5

plot 'symbol-lengths.dat' \
     with boxes lc rgb"green" \
     title "Symbol Lengths"
