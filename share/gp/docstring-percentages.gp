set terminal svg
set output "docstring-percentages.svg"

set style fill solid 0.5
set boxwidth .8 absolute

set xlabel "Programmatic definitions"
unset xtics
set yrange [0:100]
set ylabel "Documented definitions percentage"
set grid y

plot "docstring-percentages.dat" \
     using 2 with boxes lc rgb"green" title "Documentation percentages", \
     "" using 0:2:1 with labels rotate by 75 left offset char 0,.5 notitle
