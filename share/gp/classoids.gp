set terminal svg
set output "classoids.svg"

set style fill solid 0.5
set boxwidth .8 absolute

# #### FIXME: shouldn't be done by hand.
set xtics nomirror ("Structures" 1.5, "Classes" 6.5, "Conditions" 11.5)
set ylabel "Average number of direct ..."

position (i) = column (0) + 5*(i-2)

plot for [i=2:4] "classoids.dat" \
     using (position(i)):(column(i)) with boxes lc rgb"green" notitle, \
     for [i=2:4] "" \
     using (position(i)):(column(i)):1 \
     with labels rotate by 75 left offset char 0,0.5 notitle
