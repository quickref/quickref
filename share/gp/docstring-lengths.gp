set terminal svg
set output 'docstring-lengths.svg'

set style fill solid 0.5
set xlabel "Docstring length (log)"
set ylabel "Documented definitions number (log)"
set logscale x
set logscale y

plot 'docstring-lengths.dat' \
     with boxes lc rgb"green" \
     title "Docstring Lengths"
