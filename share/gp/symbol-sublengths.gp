set terminal svg
set output 'symbol-sublengths.svg'

set style fill solid 0.5
set xlabel "Symbol part length"
set ylabel "Symbols number"
set xtics 5

plot 'symbol-sublengths.dat' \
     with boxes lc rgb"green" \
     title "Symbol Part Lengths"
