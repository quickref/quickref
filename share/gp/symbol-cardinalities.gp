set terminal svg
set output 'symbol-cardinalities.svg'

set style fill solid 0.5
set xlabel "Symbol cardinality"
set ylabel "Symbols number"
set xtics 1

plot 'symbol-cardinalities.dat' \
     with boxes lc rgb"green" \
     title "Symbol Cardinalities"
