;;; fr.epita.lrde.quickref.setup.asd --- Quickref setup ASDF system definition

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:defsystem :fr.epita.lrde.quickref.setup
  :long-name "Quick Lisp Reference Manuals, setup library"
  :description "Quickref's preload setup"
  :long-description "\
The Quickref setup library provides support for various preload configuration
parameters. For a more complete description of Quickref, see the
`fr.epita.lrde.quickref' system."
  :author "Didier Verna"
  :maintainer ("Didier Verna" "Antoine Martin")
  :mailto "quickref-devel@common-lisp.net"
  :homepage
  "http://www.lrde.epita.fr/~didier/software/lisp/typesetting.php#quickref"
  :source-control "https://gitlab.common-lisp.net/quickref/quickref"
  :license "BSD"
  :components ((:file "setup")))

(asdf:defsystem :fr.epita.lrde.quickref.setup/parallel
  :long-name "Quick Lisp Reference Manuals, parallel setup library"
  :description "Quickref's automatic configuration for parallel support"
  :long-description "\
This is a virtual subsystem of the Quickref setup one (no actual code).
Its purpose is only to autodetect parallel support and update Quickref's
preload configuration on load. For a more complete description of Quickref,
see the `fr.epita.lrde.quickref' system."
  :author "Didier Verna"
  :maintainer ("Didier Verna" "Antoine Martin")
  :mailto "didier@didierverna.net"
  :homepage
  "http://www.lrde.epita.fr/~didier/software/lisp/typesetting.php#quickref"
  :source-control "https://gitlab.common-lisp.net/quickref/quickref"
  :license "BSD"
  :depends-on (:fr.epita.lrde.quickref.setup)
  :perform (load-op (o c)
	     (declare (ignore o c))
	     (call-function "fr.epita.lrde.quickref.setup:setup-parallel")))

;;; fr.epita.lrde.quickref.setup.asd ends here
