;;; package.lisp --- Quickref package definition

;; Copyright (C) 2018, 2019,2024 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :cl-user)


;; -------------------
;; Package definition:
;; -------------------

(defpackage :fr.epita.lrde.quickref
  (:documentation "The Quickref package.")
  (:use :cl :fr.epita.lrde.quickref.setup)
  (:import-from :fr.epita.lrde.quickref.cmark
    :cmark-markdown-to-html)
  (:export
    ;; From the :fr.epita.lrde.quickref.setup package:
    :*release-major-level*
    :*release-minor-level*
    :*release-status*
    :*release-status-level*
    :*release-name*
    :version
    ;; From package.lisp (this file):
    :nickname-package
    ;; From src/quickref.lisp:
    :build))

(in-package :fr.epita.lrde.quickref)


;; ------------------
;; External Utilities
;; ------------------

(defun nickname-package (&optional (nickname :quickref))
  "Add NICKNAME (:QUICKREF by default) to the :FR.EPITA.LRDE.QUICKREF package."
  (rename-package :fr.epita.lrde.quickref
		  (package-name :fr.epita.lrde.quickref)
		  (adjoin nickname (package-nicknames :fr.epita.lrde.quickref)
			  :test #'string-equal)))

;;; package.lisp ends here
