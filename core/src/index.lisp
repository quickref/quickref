;;; index.lisp --- Web indexes construction

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; Local Utilities
;; ==========================================================================

(defvar *indexes* '("library" "author") "The list of HTML indexes.")

(defun index-file-name (kind)
  "Return \"index-per-KIND\"."
  (concatenate 'string "index-per-" kind))

(defun render-index-header
    (cohort manuals-number time texinfo-version index-name)
  "Render index file's header to standard output.
The header also advertise MANUALS-NUMBER and INDEX-NAME."
  (format t (file-contents (merge-pathnames #p"index-header.html"
					    *templates-directory*))
    cohort
    (ql-dist:version (quicklisp-distribution))
    manuals-number
    time
    (version :long)
    (net.didierverna.declt:version :long)
    texinfo-version
    index-name))

(defun column-lengths (number)
  "Spread NUMBER of entries in 3 different columns as uniformly as possible.
Return the column lengths as 3 values."
  (multiple-value-bind (quotient remainder) (floor number 3)
    (case remainder
      (0 (values quotient quotient quotient))
      (1 (values (+ quotient 1) quotient quotient))
      (2 (values (+ quotient 1) (+ quotient 1) quotient)))))

(defun render-index-entry (character entries number renderer)
  "Render index file's first NUMBER ENTRIES under CHARACTER.
This function arranges ENTRIES to be rendered in 3 columns, vertically from
left to right, but taking care of vertical justification as much as possible:
the heights of the 3 columns may only differ by one, meaning that only the
last row may have less than 3 entries.

RENDERER is the function used to render the entries.
Rendering is done on *STANDARD-OUTPUT*."
  (format t "~%      <tr><th><a name=\"~A\">~A</a></th></tr>~%"
    (if (char= character #\#) "other" character)
    character)
  (multiple-value-bind (l1 l2 l3) (column-lengths number)
    (loop :for entries-1 :on entries
	  :for entries-2 :on (nthcdr l1 entries)
	  :for entries-3 :on (nthcdr (+ l1 l2) entries)
	  ;; #### WARNING: do this last so that the manual names pointers are
	  ;; correct in the :FINALLY clause, and even if the :DO clause is not
	  ;; executed.
	  :for lines :from 1 :upto l3
	  ;; #### FIXME: this call still be improved with FORMAT list
	  ;; arguments for multiple cells
	  :do (progn
		(format t "      <tr>~%")
		(funcall renderer (car entries-1))
		(funcall renderer (car entries-2))
		(funcall renderer (car entries-3))
		(format t "      </tr>~%"))
	  :finally (cond ((> l1 l2)
			  (format t "      <tr>~%")
			  (funcall renderer (car entries-1))
			  (format t "      </tr>~%"))
			 ((> l2 l3)
			  (format t "      <tr>~%")
			  (funcall renderer (car entries-1))
			  (funcall renderer (car entries-2))
			  (format t "      </tr>~%")))))
  (values))



;; ==========================================================================
;; Library Index
;; ==========================================================================

(defun library-index-character (entries)
  "Return the next library index character for ENTRIES.
ENTRIES should be a list of system names.
The next index character is the first (upcased) letter of the first system
name in ENTRIES trimmed if it's alphabetic, or # otherwise.
This function is used as the INDEX-CHARACTER-GETTER argument to
BUILD-INDEX-FILE."
  (when entries
    (let ((char (aref (trim (first entries)) 0)))
      (if (alpha-char-p char) (char-upcase char) #\#))))

(defun render-library-index-entry (entry)
  "Render a library index ENTRY.
ENTRY should be a manual name. Rendering is done on *STANDARD-OUTPUT*.
This function is used as the RENDERER argument to RENDER-INDEX-ENTRY."
  (format t "	<td></td><td><a href=\"~A.html\">~A</a></td>~%" entry entry))

(defun library-index-entries (db)
  "Return a sorted list of library names from DB.
DB is a list of elements of the form (SYSTEM-NAME ...).
Sorting is done on trimmed names."
  (sort (mapcar #'car db) #'string< :key #'trim))



;; ==========================================================================
;; Author Index
;; ==========================================================================

(defun author-index-character (entries)
  "Return the next author index character for ENTRIES.
ENTRIES should be a list of lists of the form (AUTHOR-NAME SYSTEM-NAME...).
The next index character is the first (upcased) letter of the first author
name if it's alphabetic, or # otherwise.
This function is used as the INDEX-CHARACTER-GETTER argument to
BUILD-INDEX-FILE."
  (when entries
    (let ((char (aref (car (first entries)) 0)))
      (if (alpha-char-p char) (char-upcase char) #\#))))

(defun render-author-index-entry (entry)
  "Render an author index ENTRY.
ENTRY should be a list of the form (AUTHOR-NAME SYSTEM-NAME...).
Rendering is done on *STANDARD-OUTPUT*.
This function is used as the RENDERER argument to RENDER-INDEX-ENTRY."
  (format t "        <td></td>
	<td class=\"author-entry\">
	  <table>
	    <tr><td class=\"author-name\">~A</td></tr>
~{            <tr><td><a href=\"~a.html\">~:*~a</a></td></tr>~%~}	  </table>
	</td>~%"
    (car entry) (cdr entry)))

(defun author-index-entries (db &aux (hash (make-hash-table :test 'equal)))
  "Return a sorted list of library authors from DB.
DB is a list of elements of the form (SYSTEM-NAME ... :CONTACTS C ...),
where C is a Declt report contacts list (which see).

Each element in the returned list of the form (AUTHOR-NAME SYSTEM-NAME...).
The list is sorted by lexicographic order of AUTHOR-NAMES, and for each
author, SYSTEM-NAMEs are sorted by lexicographic order of trimmed names."
  (loop :for entry :in db
	:for system-name := (car entry)
	:do (dolist (author (or (remove-if #'null
				    (mapcar #'car
				      (cadr (member :contacts entry))))
				(list "#### Unknown")))
	      (setf (gethash author hash)
		    (pushnew system-name (gethash author hash)
			     :test #'string=))))
  (sort (loop :for author :being :the :hash-keys :in hash
		:using (:hash-value system-names)
	      ;; #### NOTE: whether we want to sort manuals on trimmed names
	      ;; here is questionable. The lists in the author index are not
	      ;; that long after all.
	      :collect (cons author (sort system-names #'string< :key #'trim)))
      #'string< :key #'car))


;; ==========================================================================
;; Entry point
;; ==========================================================================

(defun build-index-file
    (kind time db context
     ;; #### NOTE: ENTRIES was originally a LOOP local variable, which causes
     ;; problems with the current implementation of the author index, because
     ;; at that time, *STANDARD-OUTPUT* is already redirected to the index
     ;; file (see comment at the top of the author index code.
     &aux (entries (funcall (intern (format nil "~:@(~A~)-INDEX-ENTRIES" kind)
				    :fr.epita.lrde.quickref)
		     db))
	  (index-character-getter
	   (intern (format nil "~:@(~A~)-INDEX-CHARACTER" kind)
		   :fr.epita.lrde.quickref))
	  (index-entry-renderer
	   (intern (format nil "RENDER-~:@(~A~)-INDEX-ENTRY" kind)
		   :fr.epita.lrde.quickref)))
  "Build Quickref's KIND index file for DB generated on TIME in CONTEXT.
The index file is called \"index-per-KIND.html\"."
  (format t "Building ~A index file...~%" kind)
  (with-open-file (*standard-output* (html-file (index-file-name kind) context)
		   :direction :output
		   :if-exists :supersede
		   :if-does-not-exist :create
		   :external-format :utf-8)
    (loop :with index-character := (funcall index-character-getter entries)
	  :with next-entries := (cdr entries)
	  :with length := 1
	  :initially (render-index-header
		      (cohort context)
		      (length db) time (texinfo-version context)
		      (format nil "~:(~A index~)" kind))
	  :while entries
	  :if (and next-entries
		   (char= index-character
			  (funcall index-character-getter next-entries)))
	    :do (setq length (1+ length) next-entries (cdr next-entries))
	  :else
	    :do (progn
		  (render-index-entry index-character entries length
				      index-entry-renderer)
		  (setq length 1
			entries next-entries
			index-character (funcall index-character-getter entries)
			next-entries (cdr entries))))
    (format t "    </table>~%  </body>~%</html>~%")))

(defun build-index-files
    (context &aux (time (net.didierverna.declt::current-time-string))
	  ;; #### NOTE: we don't want to index non-existent manuals. The
	  ;; database files are created by the Declt script right after the
	  ;; assessment stage, but things can still go wrong afterwards.
	  (db (remove-if-not
		  (lambda (system-name)
		    (uiop:file-exists-p (html-file system-name context)))
		  (mapcar #'uiop:safe-read-file-form
		    (directory
		     (merge-pathnames "*.db"
				      (truename (db-directory context)))))
		:key #'car)))
  "Build CONTEXT's index files."
  (dolist (index *indexes*) (build-index-file index time db context))
  (with-open-file (*standard-output* (html-file "index" context)
		   :direction :output
		   :if-exists :supersede
		   :if-does-not-exist :create
		   :external-format :utf-8)
    (format t "~
<meta http-equiv=\"refresh\" content=\"0;url=index-per-library.html\">~%")))

;;; index.lisp ends here
