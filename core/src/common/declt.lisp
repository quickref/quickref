;;; declt.lisp --- Texinfo generation part

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory
;; Copyright (C) 2024 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


(defvar *declt-script-name*
  (namestring (merge-pathnames #p"declt" *bin-directory*))
  "Quickref's Declt script.")

(defun declt-log-file (system-name context)
  "Return pathname for ASDF SYSTEM-NAME's Declt log file in CONTEXT."
  (merge-pathnames (make-pathname :name system-name :type "log")
		   (declt-logs-directory context)))

(defun run-declt
    (system-name context &aux (log-errors (eq (error-behavior context) :log)))
  "Run Declt on ASDF SYSTEM-NAME in CONTEXT. Return T on success."
  (multiple-value-bind (output error-output status)
      (uiop:run-program
       `(,sb-ext:*runtime-pathname*
	 "--script" ,*declt-script-name*
	 ,system-name
	 "--quicklisp-setup" ,(namestring (ql-setup:qmerge "setup.lisp"))
	 ,@(when (eq (cache-policy context) :local)
	     `("--cache-directory" ,(namestring (cache-directory context))))
	 "--texinfo-directory" ,(namestring (texinfo-directory context))
	 "--db-directory" ,(namestring (db-directory context))
	 ,@(when (cohort context)
	     `("--cohort-directory"
	       ,(namestring (cohort-db-directory context)))))
       :output (if log-errors nil *standard-output*)
       :error-output (if log-errors :string *error-output*)
       :ignore-error-status log-errors)
    (declare (ignore output))
    (unless (zerop status)
      ;; #### NOTE: if we reach that point (that is, without an error
      ;; signaled, it means that we're logging errors.
      (with-open-file (log (declt-log-file system-name context)
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create
		       :external-format :utf-8)
	(format log "~A~%" error-output)))
    (zerop status)))

;;; texinfo.lisp ends here
