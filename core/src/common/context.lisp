;;; context.lisp --- Quickref execution context

;; Copyright (C) 2022, 2024 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; Context Class
;; ==========================================================================

(defclass context ()
  ((cohort :documentation "Whether to create a cohort."
	   :initarg :cohort :reader cohort)
   (output-directory :documentation "Quickref's output directory."
		     :initarg :output-directory :reader output-directory)
   (makeinfo-path :documentation "Path to the 'makeinfo' executable."
		  :initarg :makeinfo-path :reader makeinfo-path)
   (texinfo-version :documentation "Texinfo version."
		    :reader texinfo-version)
   (cross-reference-validation
    :documentation  "Whether to validate cross references."
    :initarg :cross-reference-validation :reader cross-reference-validation)
   (error-behavior
    :documentation "Behavior to adopt when errors are encountered.
If :log, just log the errors and continue processing. Otherwise, stop at the
first error. Note also that when errors are logged (which is meant to be the
non-interactive way of running Quickref), normal output from Declt and
Makeinfo is discarded completely."
    :initarg :error-behavior :reader error-behavior)
   (cache-policy
    :documentation  "Compilation cache handling policy.
If :global, the usual ASDF cache is used, meaning that compilation caches
are shared across Declt calls. Otherwise, every Declt call uses a different
compilation cache."
    :initarg :cache-policy :reader cache-policy)
   (empty-cache
    :documentation "Whether to empty the compilation cache before proceeding.
This parameter is only considered for a local cache policy. The global ASDF
cache is never emptied."
    :initarg :empty-cache :reader empty-cache))
  (:documentation "The Context class."))

(defmethod initialize-instance :after
    ((context context)
     &key
     &aux (texinfo-version
	   (uiop:run-program `(,(makeinfo-path context) "--version")
	     :output :string)))
  "Initialize CONTEXT's Texinfo version."
  (setf (slot-value context 'texinfo-version)
	(subseq texinfo-version 0 (position #\Newline texinfo-version))))

(defun make-context
    (cohort output-directory
     makeinfo-path cross-reference-validation
     error-behavior
     cache-policy empty-cache)
  "Create and return a new context."
  (make-instance 'context
    :cohort cohort
    :output-directory output-directory
    :makeinfo-path makeinfo-path
    :cross-reference-validation cross-reference-validation
    :error-behavior error-behavior
    :cache-policy cache-policy
    :empty-cache empty-cache))


;; ==========================================================================
;; Pseudo-Accessors
;; ==========================================================================

(defun logs-directory (context)
  "Return CONTEXT's logs directory."
  (merge-pathnames #p"logs/" (output-directory context)))

(defun declt-logs-directory (context)
  "Return CONTEXT's Declt logs directory."
  (merge-pathnames #p"declt/" (logs-directory context)))

(defun makeinfo-logs-directory (context)
  "Return CONTEXT's Makeinfo logs directory."
  (merge-pathnames #p"makeinfo/" (logs-directory context)))

(defun db-directory (context)
  "Return CONTEXT's database files directory."
  (merge-pathnames #p"db/" (output-directory context)))

(defun cohort-directory (context)
  "Return CONTEXT's cohort files directory."
  (merge-pathnames #p"cohort/" (output-directory context)))

(defun cohort-db-directory (context)
  "Return CONTEXT's cohort database files directory."
  (merge-pathnames #p"db/" (cohort-directory context)))

(defun cohort-stats-directory (context)
  "Return CONTEXT's cohort statistics files directory."
  (merge-pathnames #p"stats/" (cohort-directory context)))

(defun texinfo-directory (context)
  "Return CONTEXT's Texinfo files directory."
  (merge-pathnames #p"texinfo/" (output-directory context)))

(defun html-directory (context)
  "Return CONTEXT's generated HTML files directory."
  (merge-pathnames #p"html/" (output-directory context)))

(defun html-cohort-directory (context)
  "Return CONTEXT's generated HTML cohort files directory."
  (merge-pathnames #p"cohort/" (html-directory context)))

(defun cache-directory (context)
  "Return CONTEXT's compilation cache root directory.
This is the location used when the cache policy is local. ASDF and Declt
get their own subdirectory, every documented system also has its own one,
leading to duplicate compilation of every shared dependency."
  (merge-pathnames #p"cache/" (output-directory context)))



;; ==========================================================================
;; Context-Related Utilities
;; ==========================================================================

(defun html-file (name context)
  "Return pathname for NAME.html file in CONTEXT."
  (merge-pathnames (make-pathname :name name :type "html")
		   (html-directory context)))

;;; context.lisp ends here
