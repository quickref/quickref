;;; readme.lisp --- README detection and insertion

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Comment:

;; #### NOTE: hopefully, this will all go away once Declt is able to do this
;; #### by itself.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; README Detection
;; ==========================================================================

(defun readme-file (system-name)
  "Return the pathname of a README(.*) file for ASDF SYSTEM-NAME, if any.
Try the system's directory first. If that fails and the system is part of a
Quicklisp RELEASE, also try the release's directory (which may be different)."
  (first
   (or (multiple-value-bind (foundp found-system pathname)
	   ;; #### NOTE: we don't use ASDF:SYSTEM-SOURCE-DIRECTORY here
	   ;; because it may fail when the system is missing some
	   ;; dependencies. Remember that we don't want anything else than the
	   ;; system's location here. Actual loading belongs to the Declt
	   ;; script.
	   (asdf:locate-system system-name)
	 (declare (ignore foundp found-system))
	 (when pathname
	   (directory (merge-pathnames "README.*"
				       (directory-namestring pathname)))))
       (let ((release (ql-dist:release (ql-dist:find-system-in-dist
					system-name
					(quicklisp-distribution)))))
	 (when release
	   (directory (merge-pathnames "README.*"
				       (ql-dist:base-directory release))))))))



;; ==========================================================================
;; README Insertion
;; ==========================================================================

(defun markdown-to-html (string)
  "Return Markdown STRING formatted as HTML.
Try our CommonMark FFI first, fall back to 3BMD otherwise."
  (handler-case (cmark-markdown-to-html string (length string) 0)
    (error ()
      (with-output-to-string (out)
	(3bmd:parse-string-and-print-to-stream string out)))))

(defun <pre> (string)
  "Return STRING as HTML <pre>."
  (format nil "<pre style=\"white-space: pre-wrap;\">~%~A~%</pre>" string))

(defun readme-to-html (readme-file)
  "Return contents of README-FILE formatted as HTML."
  (let ((contents (file-contents readme-file)))
    (if (member (string-downcase (pathname-type readme-file))
		'("md" "markdown" "mdown" "mkdn" "mdwn" "mkd")
		:test #'string=)
      (markdown-to-html contents)
      (<pre> contents))))

(defun insert-readme-file (html-file readme-file)
  "Insert the contents of README-FILE into HTML-FILE.
README-FILE contents is rendered in HTML and inserted as introduction."
  (let ((html-contents (file-contents html-file)))
    (with-open-file
	(*standard-output* html-file
	 :direction :output :if-exists :supersede :external-format :utf-8)
      (write-string
       (cl-ppcre:regex-replace
	"\\<h2 class=\\\"chapter\\\"\\>1 Introduction\\<\\/h2\\>\\n"
	html-contents
	;; #### FIXME: we're gonna have problems if the README file is not in
	;; UTF8...
	(list :match (readme-to-html readme-file)))))))

;;; readme.lisp ends here
