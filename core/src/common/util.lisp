;;; util.lisp --- Common utilities

;; Copyright (C) 2022 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; Quickref Distribution Utilities
;; ==========================================================================

(defvar *top-directory* (asdf:system-source-directory :fr.epita.lrde.quickref)
  "Quickref's top directory.")

(defvar *bin-directory* (merge-pathnames #p"bin/" *top-directory*)
  "Quickref's bin directory.")

(defvar *share-directory* (merge-pathnames #p"share/" *top-directory*)
  "Quickref's share directory.")

(defvar *css-directory* (merge-pathnames #p"css/" *share-directory*)
  "Quickref's CSS directory.")

(defvar *gp-directory* (merge-pathnames #p"gp/" *share-directory*)
  "Quickref's Gnuplot directory.")

(defvar *templates-directory* (merge-pathnames #p"templates/" *share-directory*)
  "Quickref's templates directory.")



;; ==========================================================================
;; Standard Wanabees
;; ==========================================================================

(defmacro while (test &rest body)
  "The WHILE loop."
  `(do () ((not ,test))
     ,@body))



;; ==========================================================================
;; File System Utilities
;; ==========================================================================

(defun file-contents (file)
  "Attempt to safely read FILE into a string and return it.
Safely means try to detect a proper encoding."
  (handler-case (uiop:read-file-string file)
    (error ()
      (handler-case (uiop:read-file-string file :external-format :latin-1)
	(error ()
	  (uiop:read-file-string file
	     :external-format '(:utf-8 :replacement #\?)))))))



;; ==========================================================================
;; Quicklisp Utilities
;; ==========================================================================

;; #### NOTE: I find Quicklisp's object-orientation quite weird. Many objects
;; are created on demand, with slots sometimes unbound and methods on
;; SLOT-UNBOUND returning values instead (not even updating the slot in
;; question). As a result, there may be many different objects lying around at
;; the same time, yet representing the same thing in different
;; states. Example: FIND-DIST always returns a new object when you call it,
;; instead of consistently returning a single object per distribution. As a
;; result, the objects that you get aren't updated when the distribution is,
;; and e.g., you may end up with a wrong release date. That's why we use a
;; function instead of a variable below.
(defun quicklisp-distribution ()
  "Return a Quicklisp distribution object for the \"quicklisp\" distribution."
  (ql-dist:find-dist "quicklisp"))

;; #### FIXME: currently, we document one (primary) system per release. Most
;; of the time, it's the correct thing to do because:
;; - Declt automatically documents sub-systems as part of the primary one,
;; - we usually don't want to document secondary systems such as test ones.
;; In some situations, we will miss systems that we could document
;; however. For example, a library having a contrib directory may provide
;; additional and interesting primary systems. More importantly, some releases
;; provide a set of libraries (e.g. cl-gtk2 provides gtk, gdk, pango etc.)
;; that we should all document. It is impossible to guess all this
;; properly. What we could perhaps do is maintain a list of particular
;; releases for which we explicitly say which systems are primary, and fall
;; back to the below heuristics otherwise.
;; One additional note: currently, the files are named after the ASDF
;; system. We could think of using the (nicer) release name, on the condition
;; that only one system per release is documented, as it is the case
;; now. Later on, we could have a release entry with several manuals in it.
(defun primary-system (release)
  "Return RELEASE's primary system.
The primary system is determined heuristically as follows:
- it's the only system,
- it has the same name as the release,
- it has the same name as the release, with \"cl-\" prefixed removed,
- it has the shortest name."
  (let ((release-name (ql-dist:name release))
	(systems (ql-dist:provided-systems release)))
    (or (when (= (length systems) 1) (car systems))
	(find release-name systems :key #'ql-dist:name :test #'string=)
	(find (trim release-name) systems :key #'trim :test #'string=)
	(first (sort systems
		     (lambda (name1 name2) (< (length name1) (length name2)))
		     :key #'ql-dist:name)))))

(defgeneric library-name (library)
  (:documentation "Return LIBRARY's name.")
  (:method ((system-name string))
    "Return ASDF SYSTEM-NAME."
    system-name)
  (:method ((system ql-dist:system))
    "Return Quicklisp SYSTEM's name."
    (ql-dist:name system))
  (:method ((release ql-dist:release))
    "Return Quicklisp RELEASE's primary system's name."
    (library-name (primary-system release))))

(defgeneric trim (object)
  (:documentation "Remove potential CL- prefix from OBJECT's name.")
  (:method ((string string))
    "Remove potential CL- prefix from STRING."
    (if (and (> (length string) 3) (string= (subseq string 0 3) "cl-"))
      (subseq string 3)
      string))
  (:method ((system ql-dist:system))
    "Remove potential CL- prefix from SYSTEM's name."
    (trim (ql-dist:name system)))
  (:method ((release ql-dist:release))
    "Remove potential CL- prefix from RELEASE's name."
    (trim (ql-dist:name release))))

;;; util.lisp ends here
