;;; sequential.lisp --- Sequential build algorithm

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Comment:



;;; Code:

(in-package :fr.epita.lrde.quickref)


(defmacro with-progression ((context message &rest arguments) &body body)
  "Execute BODY with a progression indicator in CONTEXT. Return BODY's value.
MESSAGE and ARGUMENTS are passed to FORMAT to print the progression.
If CONTEXT logs errors, the indicator is then followed by an elision,
eventually terminated by either ok or ko upon completion."
  (let ((log (gensym "log"))
	(result (gensym "result")))
    `(let ((,log (eq (error-behavior ,context) :log))
	   ,result)
       (format t ,(concatenate 'string message "~:[~%~;... ~]")
	 ,@arguments ,log)
       (finish-output)
       (setq ,result (multiple-value-list (progn ,@body)))
       (when ,log (format t "~:[ko~;ok~]~%" (car ,result)))
       (values-list ,result))))

(defun build-manual
    (library context &aux (system-name (library-name library)))
  "Build LIBRARY's reference manual in CONTEXT.
Return :html, :texi or NIL, indicating how far the process went."
  (format t "Building ~A manual...~%" system-name)
  ;; #### WARNING: this asymmetry is not nice, but it will go away once Declt
  ;; itself is capable of README insertion.
  (when (with-progression (context "  Running Declt")
	  (run-declt system-name context))
    (cond ((with-progression (context "  Running Makeinfo")
	     (run-makeinfo system-name context))
	   (let ((readme-file (readme-file system-name)))
	     (when readme-file
	       (with-progression (context "  Inserting README file")
		 (insert-readme-file (html-file system-name context)
				     readme-file))))
	   :html)
	  (t :texi))))

(defmethod build-manuals (libraries context (parallel (eql nil)))
  "Sequential version."
  (loop :with total := (length libraries)
	:and htmls := 0
	:and texis := 0
	:for count :from 1
	:for library :in libraries
	:do (case (progn (format t "(~D/~D) " count total)
			 (build-manual library context))
	      (:texi (incf texis))
	      (:html (incf texis) (incf htmls)))
	:finally (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
		   htmls total (- total texis) (- texis htmls)))
  (values))

;;; sequential.lisp ends here
