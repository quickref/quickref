;;; parallel.lisp --- Parallel build algorithms

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Comment:


;;; Code:

(in-package :fr.epita.lrde.quickref)


(defvar *library-queue* nil
  "The list of libraries ready to be processed.")

(defvar *texinfo-queue* nil
  "The list of libraries for which Texinfo files are ready to be processed.")

(defclass context-// (context)
  ((declt-threads :documentation "The number of Declt threads."
		  :initarg :declt-threads :reader declt-threads)
   (makeinfo-threads :documentation "The number of Makeinfo threads."
		     :initarg :makeinfo-threads :reader makeinfo-threads))
  (:documentation "The context-// class."))



;; ==========================================================================
;; Local Utilities
;; ==========================================================================

(defmacro endpush (object place)
  "Like PUSH, but at the end."
  `(setf ,place (nconc ,place (list ,object))))

(defmacro format-// (stream &rest args)
  "Like FORMAT, but block until all output is flushed."
  (let ((the-stream (gensym "stream")))
    `(let ((,the-stream ,stream))
       (format ,the-stream ,@args)
       (finish-output ,the-stream))))


;; ----------------
;; Dependency Graph
;; ----------------

(defun remove-standalone-systems (graph &aux names)
  "Remove standalone systems (systems with no dependencies) from GRAPH.
Return the list of removed systems names."
  (maphash (lambda (key value)
	     (unless value
	       (push key names)
	       (remhash key graph)))
	   graph)
  names)

(defun remove-dependencies (graph dependencies)
  "Remove DEPENDENCIES from all GRAPH's dependency lists. Return DEPENDENCIES."
  (maphash (lambda (key value)
	     (setf (gethash key graph)
		   (delete-if (lambda (name)
				(member name dependencies :test #'string=))
			      value)))
	   graph)
  dependencies)

(defun dependency-graph-size (graph)
  "Return the number of systems in dependency GRAPH."
  (hash-table-count graph))

(defun primary-systems-dependency-graph
    (releases
     &aux (graph (make-hash-table :test #'equal :size (length releases)))
	  names)
  "Return RELEASES' primary systems dependency graph.
This is a hash table mapping system names to their list of dependencies (also
system names). Only internal dependencies (that is, across RELEASES's primary
systems) are preserved."
  (dolist (system (mapcar #'primary-system releases))
    (let ((name (ql-dist:name system)))
      (push name names)
      (setf (gethash name graph)
	    (copy-list (ql-dist:required-systems system)))))
  (maphash (lambda (key value)
	     (setf (gethash key graph)
		   (delete-if-not (lambda (name)
				    (member name names :test #'string=))
				  value)))
	   graph)
  graph)



;; ==========================================================================
;; Solution 1
;; ==========================================================================

;; In this solution, the main thread builds the Texinfo files sequentially,
;; and a second thread waits for them, grabs them (possibly in batch), and
;; converts them to HTML. Because the libraries are processed sequentially by
;; Declt, this version can be used with both local and global caching
;; policies.

(defun build-html-manuals-//-1 (context total texinfo-queue new-texinfo)
  "Build *TEXINFO-QUEUE* HTML manuals in CONTEXT, parallel version #1.
Return the number of HTML files successfully built."
  (loop :with htmls := 0
	:with libraries
	:do (sb-thread:with-mutex (texinfo-queue)
	      (unless *texinfo-queue*
		(sb-thread:condition-wait new-texinfo texinfo-queue))
	      (setq libraries *texinfo-queue* *texinfo-queue* nil))
	;; #### WARNING: the test below is necessary because spurious wakeups
	;; from a condition wait may occur, without the condition actually
	;; being true.
	:when libraries
	  :do (progn
		(format-// t "Got ~D Texinfo file~:P to batch process.~%"
		  (- (length libraries)
		     (if (eq (car (last libraries)) :done) 1 0)))
		(dolist (library libraries)
		  (when (eq library :done)
		    (return-from build-html-manuals-//-1 htmls))
		  (let ((system-name (library-name library)))
		    (cond ((run-makeinfo system-name context)
			   (let ((readme-file (readme-file system-name)))
			     (when readme-file
			       (insert-readme-file
				(html-file system-name context)
				readme-file))
			     (incf htmls)
			     (format-// t
				 "(~D/~D) ~A.html...~:[~; README...~] ok.~%"
			       htmls total system-name readme-file)))
			  (t
			   (format-// t "(~D/~D) ~A.html... ko.~%"
			     (1+ htmls) total system-name))))))))

(defun build-texinfo-manuals-//-1 (libraries context texinfo-queue new-texinfo)
  "Build LIBRARIES Texinfo manuals in CONTEXT, parallel version #1.
Return the number of Texinfo files successfully built."
  (loop :with total := (length libraries)
	:and texis := 0
	:for count :from 1
	:for library :in libraries
	:for system-name := (library-name library)
	:do (cond ((run-declt system-name context)
		   (format-// t "(~D/~D) ~A.texi... ok.~%"
		     count total system-name)
		   (incf texis)
		   (sb-thread:with-mutex (texinfo-queue)
		     (endpush library *texinfo-queue*)
		     (sb-thread:condition-notify new-texinfo)))
		  (t
		   (format-// t "(~D/~D) ~A.texi... ko.~%"
		     count total system-name)))
	:finally (progn (sb-thread:with-mutex (texinfo-queue)
			  (endpush :done *texinfo-queue*)
			  (sb-thread:condition-notify new-texinfo))
			(return texis))))

(defmethod build-manuals
    (libraries context (parallel (eql 1)) &aux (total (length libraries)))
  "Parallel version #1."
  (setq *texinfo-queue* nil)
  (let* (texis
	 htmls
	 (texinfo-queue (sb-thread:make-mutex))
	 (new-texinfo (sb-thread:make-waitqueue))
	 (thread (sb-thread:make-thread
		  (lambda (context)
		    (build-html-manuals-//-1
		     context total texinfo-queue new-texinfo))
		  :arguments (list context))))
    (setq texis (build-texinfo-manuals-//-1
		 libraries context texinfo-queue new-texinfo)
	  htmls (sb-thread:join-thread thread))
    (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
      htmls total (- total texis) (- texis htmls)))
  (values))



;; ==========================================================================
;; Solution 2
;; ==========================================================================

;; In this solution, the main process spawns several threads building Texinfo
;; files in parallel, and several others waiting for them (one by one this
;; time)), and converting them to HTML. Because the libraries are processed in
;; parallel by Declt, this version can only be used with a local caching
;; policy.

(defun build-html-manuals-//-2
    (context thread-number texinfo-queue new-texinfo)
  "Build *TEXINFO-QUEUE* HTML manuals in CONTEXT, parallel version #2.
Return the number of HTML files successfully built."
  (loop :with htmls := 0
	:for count :from 1
	:for library
	  := (sb-thread:with-mutex (texinfo-queue)
	       (unless *texinfo-queue*
		 (sb-thread:condition-wait new-texinfo texinfo-queue))
	       (if (eq (car *texinfo-queue*) :done)
		 ;; #### WARNING: don't pop :DONE! All the HTML threads need
		 ;; to see it.
		 :done
		 (pop *texinfo-queue*)))
	:until (eq library :done)
	;; #### WARNING: the test below is necessary because spurious wakeups
	;; from a condition wait may occur, without the condition actually
	;; being true.
	:when library
	  :do (let ((system-name (library-name library)))
		(cond ((run-makeinfo system-name context)
		       (let ((readme-file (readme-file system-name)))
			 (when readme-file
			   (insert-readme-file
			    (html-file system-name context)
			    readme-file))
			 (incf htmls)
			 (format-// t "~
[Makeinfo thread #~D: ~D/~D] ~A.html...~:[~; README...~] ok.~%"
			   thread-number htmls count system-name readme-file)))
		      (t
		       (format-// t
			"[Makeinfo thread #~D: ~D/~D] ~A.html... ko.~%"
			thread-number htmls count system-name))))
	:finally (return htmls)))

(defun build-texinfo-manuals-//-2
    (context thread-number library-queue texinfo-queue new-texinfo)
  "Build *LIBRARY-QUEUE* Texinfo manuals in CONTEXT, parallel version #2.
Return the number of Texinfo files successfully built."
  (loop :with texis := 0
	:for count :from 1
	:for library := (sb-thread:with-mutex (library-queue)
			  (pop *library-queue*))
	:until (null library)
	:do (let ((system-name (library-name library)))
	      (cond ((run-declt system-name context)
		     (incf texis)
		     (format-// t  "[Declt thread #~D: ~D/~D] ~A.texi... ok.~%"
		       thread-number texis count system-name)
		     (sb-thread:with-mutex (texinfo-queue)
		       (endpush library *texinfo-queue*)
		       (sb-thread:condition-notify new-texinfo)))
		    (t
		     (format-// t "[Declt thread #~D: ~D/~D] ~A.texi... ko.~%"
		       thread-number texis count system-name))))
	:finally (return texis)))

(defmethod build-manuals
    (libraries context (parallel (eql 2)) &aux (total (length libraries)))
  "Parallel version #2."
  (setq *library-queue* libraries *texinfo-queue* nil)
  (let* ((texis 0)
	 (htmls 0)
	 (library-queue (sb-thread:make-mutex))
	 (texinfo-queue (sb-thread:make-mutex))
	 (new-texinfo (sb-thread:make-waitqueue))
	 (declt-threads
	   (loop :for thread-number :from 1 :to (declt-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     (build-texinfo-manuals-//-2
			      context thread-number
			      library-queue texinfo-queue new-texinfo))
			   :arguments (list context thread-number))))
	 (makeinfo-threads
	   (loop :for thread-number :from 1 :to (makeinfo-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     (build-html-manuals-//-2
			      context thread-number
			      texinfo-queue new-texinfo))
			   :arguments (list context thread-number)))))
    (mapc (lambda (thread) (incf texis (sb-thread:join-thread thread)))
      declt-threads)
    (sb-thread:with-mutex (texinfo-queue)
      (endpush :done *texinfo-queue*)
      (sb-thread:condition-broadcast new-texinfo))
    (mapc (lambda (thread) (incf htmls (sb-thread:join-thread thread)))
      makeinfo-threads)
    (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
      htmls total (- total texis) (- texis htmls)))
  (values))



;; ==========================================================================
;; Solution 3
;; ==========================================================================

;; This solution is similar to solution 2, except that the libraries queue is
;; itself built progressively instead of being initialized with all the
;; libraries at the beginning. More specifically, the main thread fills this
;; queue with successive batches of libraries having no more dependency to
;; compile. Because of that, every such batch can be processed in parallel, as
;; in solution 2, but even with a global cache policy.

;; We thus have two new condition variables: one for signaling a new batch of
;; libraries (awakening the texinfo threads), and one for signaling an
;; exhausted batch (awakening the main thread). The main thread will signal
;; that there are no more batches to process by pushing a :DONE flag, as we
;; already do for the texinfo queue.

(defvar *remaining-libraries-count* 0
  "The number of libraries still in processing by a Texinfo thread.")

(defun build-texinfo-manuals-//-3
    (context thread-number
     library-queue new-batch empty-batch texinfo-queue new-texinfo)
  "Build *LIBRARY-QUEUE* Texinfo manuals in CONTEXT, parallel version #3.
Return the number of Texinfo files successfully built."
  (loop :with texis := 0
	:for count :from 1
	:for library
	  := (sb-thread:with-mutex (library-queue)
	       (unless *library-queue*
		 (sb-thread:condition-wait new-batch library-queue))
	       (if (eq (car *library-queue*) :done)
		 ;; #### WARNING: don't pop :DONE! All the TEXI threads need
		 ;; to see it.
		 :done
		 (pop *library-queue*)))
	:until (eql library :done)
	;; #### WARNING: the test below is necessary because spurious wakeups
	;; from a condition wait may occur, without the condition actually
	;; being true.
	:when library
	  :do (let ((system-name (library-name library)))
		(cond ((run-declt system-name context)
		       (incf texis)
		       (format-// t "[Declt thread #~D: ~D/~D] ~A.texi... ok.~%"
			 thread-number texis count system-name)
		       (sb-thread:with-mutex (texinfo-queue)
			 (endpush library *texinfo-queue*)
			 (sb-thread:condition-notify new-texinfo)))
		      (t
		       (format-// t "[Declt thread #~D: ~D/~D] ~A.texi... ko.~%"
			 thread-number texis count system-name)))
		;; #### WARNING: we absolutely need to wait until here before
		;; awakening the main thread. Otherwise, a new batch of
		;; libraries could be installed (and its processing started in
		;; another thread) before this one is ready.
		(sb-thread:with-mutex (library-queue)
		  (decf *remaining-libraries-count*)
		  (when (zerop *remaining-libraries-count*)
		    (sb-thread:condition-notify empty-batch))))
	:finally (return texis)))

(defun cull-standalone-systems (graph)
  "Cull standalone systems from dependency GRAPH. Return them.
Standalone systems are systems with no depdendencies.
This function removes standalone systems from depdendency GRAPH and updates
all remaining systems' dependencies accordingly."
  (remove-dependencies graph (remove-standalone-systems graph)))

(defun primary-systems-batches
    (releases &aux (graph (primary-systems-dependency-graph releases)))
  "Return a list of primary systems batches (also lists) from RELEASES.
The list is ordered by dependencies: each batch contains only systems
depending on those from the previous ones (thus, the first batfch contains
systems having no dependency)."
  (loop :with batches
	:until (zerop (dependency-graph-size graph))
	:do (setq batches
		  (append batches (list (cull-standalone-systems graph))))
	:finally (return batches)))

(defmethod build-manuals
    (libraries context (parallel (eql 3))
     &aux (total (length libraries))
	  (batches (append (primary-systems-batches libraries) '((:done)))))
  "Parallel version #3."
  (setq *library-queue* (pop batches)
	*remaining-libraries-count* (length *library-queue*)
	*texinfo-queue* nil)
  ;; #### FIXME: wrong if no batch (except the :done one)!
  (format-// t "Processing batch of ~D librar~:@P.~%" (length *library-queue*))
  (let* ((texis 0)
	 (htmls 0)
	 (library-queue (sb-thread:make-mutex))
	 (new-batch (sb-thread:make-waitqueue))
	 (empty-batch (sb-thread:make-waitqueue))
	 (texinfo-queue (sb-thread:make-mutex))
	 (new-texinfo (sb-thread:make-waitqueue))
	 (declt-threads
	   (loop :for thread-number :from 1 :to (declt-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     (build-texinfo-manuals-//-3
			      context thread-number
			      library-queue new-batch empty-batch
			      texinfo-queue new-texinfo))
			   :arguments (list context thread-number))))
	 (makeinfo-threads
	   (loop :for thread-number :from  1 :to (makeinfo-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     ;; #### NOTE: this function from the second
			     ;; solution is indeed usable as-is here.
			     (build-html-manuals-//-2
			      context thread-number texinfo-queue new-texinfo))
			   :arguments (list context thread-number)))))
    (while batches
      (sb-thread:with-mutex (library-queue)
	(unless (zerop *remaining-libraries-count*)
	  (sb-thread:condition-wait empty-batch library-queue))
	;; #### WARNING: the test below is necessary because spurious
	;; wakeups from a condition wait may occur, without the
	;; condition actually being true.
	(when (zerop *remaining-libraries-count*)
	  (setq *library-queue* (pop batches)
		*remaining-libraries-count* (length *library-queue*))
	  (unless (eq (car *library-queue*) :done)
	    (format-// t "Processing batch of ~D librar~:@P.~%"
	      (length *library-queue*)))
	  (sb-thread:condition-broadcast new-batch))))
    (mapc (lambda (thread) (incf texis (sb-thread:join-thread thread)))
      declt-threads)
    (sb-thread:with-mutex (texinfo-queue)
      (endpush :done *texinfo-queue*)
      (sb-thread:condition-broadcast new-texinfo))
    (mapc (lambda (thread) (incf htmls (sb-thread:join-thread thread)))
      makeinfo-threads)
    (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
      htmls total (- total texis) (- texis htmls)))
  (values))



;; ==========================================================================
;; Solution 4
;; ==========================================================================

;; This solution is similar to solution 3, except that instead of filling the
;; libraries pool by statically computed batches, we dynamically track the
;; libraries done by Declt, recompute potentially new leaves in the
;; dependency graph, and immediately add them to the pool.

(defvar *post-declt-queue* nil
  "The list of libraries that passed the Declt stage.
These are the same ones that are fed to the Texinfo queue, but this queue is
used to dynamically update the dependency graph by looking for potentially new
leaves and adding them to the library queue.")

(defun build-texinfo-manuals-//-4
    (context thread-number
     library-queue new-batch post-declt-queue collect-new-leaves
     texinfo-queue new-texinfo)
  "Build *LIBRARY-QUEUE* Texinfo manuals in CONTEXT, parallel version #4.
Return the number of Texinfo files successfully built."
  (loop :with texis := 0
	:for count :from 1
	:for library
	  := (sb-thread:with-mutex (library-queue)
	       (unless *library-queue*
		 (sb-thread:condition-wait new-batch library-queue))
	       (if (eq (car *library-queue*) :done)
		 ;; #### WARNING: don't pop :DONE! All the TEXI threads need
		 ;; to see it.
		 :done
		 (pop *library-queue*)))
	:until (eql library :done)
	;; #### WARNING: the test below is necessary because spurious wakeups
	;; from a condition wait may occur, without the condition actually
	;; being true.
	:when library
	  :do (let ((system-name (library-name library)))
		(cond ((run-declt system-name context)
		       (incf texis)
		       (format-// t "[Declt thread #~D: ~D/~D] ~A.texi... ok.~%"
			 thread-number texis count system-name)
		       (sb-thread:with-mutex (texinfo-queue)
			 (endpush library *texinfo-queue*)
			 (sb-thread:condition-notify new-texinfo)))
		      (t
		       (format-// t "[Declt thread #~D: ~D/~D] ~A.texi... ko.~%"
			 thread-number texis count system-name)))
		;; #### WARNING: we absolutely need to wait until here before
		;; awakening the main thread. Otherwise, a new batch of
		;; libraries could be installed (and its processing started in
		;; another thread) before this one is ready.
		(sb-thread:with-mutex (post-declt-queue)
		  (endpush library *post-declt-queue*)
		  (sb-thread:condition-notify collect-new-leaves)))
	:finally (return texis)))

(defmethod build-manuals
    (libraries context (parallel (eql 4))
     &aux (total (length libraries))
	  (graph (primary-systems-dependency-graph libraries)))
  "Parallel version #4."
  (setq *library-queue* (remove-standalone-systems graph)
	*post-declt-queue* nil
	*texinfo-queue* nil)
  ;; #### FIXME: wrong it no batch!
  (format-// t "Processing batch of ~D librar~:@P.~%" (length *library-queue*))
  (when (zerop (dependency-graph-size graph)) (endpush :done *library-queue*))
  (let* ((texis 0)
	 (htmls 0)
	 (library-queue (sb-thread:make-mutex))
	 (new-batch (sb-thread:make-waitqueue))
	 (post-declt-queue (sb-thread:make-mutex))
	 (collect-new-leaves (sb-thread:make-waitqueue))
	 (texinfo-queue (sb-thread:make-mutex))
	 (new-texinfo (sb-thread:make-waitqueue))
	 (declt-threads
	   (loop :for thread-number :from 1 :to (declt-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     (build-texinfo-manuals-//-4
			      context thread-number
			      library-queue new-batch
			      post-declt-queue collect-new-leaves
			      texinfo-queue new-texinfo))
			   :arguments (list context thread-number))))
	 (makeinfo-threads
	   (loop :for thread-number :from  1 :to (makeinfo-threads context)
		 :collect (sb-thread:make-thread
			   (lambda (context thread-number)
			     ;; #### NOTE: this function from the second
			     ;; solution is indeed usable as-is here.
			     (build-html-manuals-//-2
			      context thread-number texinfo-queue new-texinfo))
			   :arguments (list context thread-number)))))
    (while (not (zerop (dependency-graph-size graph)))
      (let (post-declt-libraries)
	(sb-thread:with-mutex (post-declt-queue)
	  (unless *post-declt-queue*
	    (sb-thread:condition-wait collect-new-leaves post-declt-queue))
	  ;; #### WARNING: the test below is necessary because spurious
	  ;; wakeups from a condition wait may occur, without the condition
	  ;; actually being true.
	  (when *post-declt-queue*
	    (setq post-declt-libraries *post-declt-queue*
		  *post-declt-queue* nil)))
	(when post-declt-libraries
	  (remove-dependencies graph post-declt-libraries)
	  (let* ((batch (remove-standalone-systems graph))
		 (batch-length (length batch)))
	    (when (zerop (dependency-graph-size graph)) (endpush :done batch))
	    (when batch
	      (sb-thread:with-mutex (library-queue)
		(unless (zerop batch-length)
		  (format-// t "Adding batch of ~D librar~:@P.~%" batch-length))
		(setq *library-queue* (append *library-queue* batch))
		(sb-thread:condition-broadcast new-batch)))))))
    (mapc (lambda (thread) (incf texis (sb-thread:join-thread thread)))
      declt-threads)
    (sb-thread:with-mutex (texinfo-queue)
      (endpush :done *texinfo-queue*)
      (sb-thread:condition-broadcast new-texinfo))
    (mapc (lambda (thread) (incf htmls (sb-thread:join-thread thread)))
      makeinfo-threads)
    (format t "~
Successfully built ~D/~D manual~:P (~D Declt error~:P, ~D Makeinfo error~:P).~%"
      htmls total (- total texis) (- texis htmls)))
  (values))

;;; parallel.lisp ends here
