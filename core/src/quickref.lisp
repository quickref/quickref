;;; quickref.lisp --- Entry point

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory
;; Copyright (C) 2024 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Comment:



;;; Code:

(in-package :fr.epita.lrde.quickref)


(defun copy-style-sheets (context)
  "Copy Quickref's style sheets to the HTML output directory in CONTEXT."
  (format t "Copying stylesheets...~%")
  (let ((style-sheets '(#p"document.css" #p"index.css")))
    (dolist (style-sheet style-sheets)
      (uiop:copy-file
       (merge-pathnames style-sheet *css-directory*)
       (merge-pathnames style-sheet (html-directory context))))))

(defvar *foreign-system-names* '("asdf" "fr.epita.lrde.quickref")
  "Non-Quicklisp systems that need to be documented.")

(defun renew-directories (&rest directories)
  "Ensure DIRECTORIES exist and empty them."
  (dolist (directory directories)
    (uiop:delete-directory-tree directory
      :validate t :if-does-not-exist :ignore)
    (ensure-directories-exist directory))
  (values))

(defun build
    (&key cohort
	  (update t)
	  (libraries :all)
	  (output-directory
	   (merge-pathnames #p"quickref/" (user-homedir-pathname)))
	  (makeinfo-path #p"makeinfo")
	  (cross-reference-validation t)
	  (error-behavior :log)
	  (cache-policy :global cache-policy-p)
	  (empty-cache t)
	  parallel
	  (declt-threads 2)
	  (makeinfo-threads 2)
     &aux context)
  "Build the Quickref website.
The following keyword arguments are available.
- COHORT: whether to create a cohort (NIL by default).
- UPDATE: whether to update Quicklisp before processing. Defaults to T.
- LIBRARIES: Quicklisp libraries to process.
  Possible values are :all (the default) and :installed. When :all, process
  (and hence download) all Quicklisp libraries. Otherwise, only process the
  libraries that are already installed.
- OUTPUT-DIRECTORY: Quickref's output directory. Defaults to ~/quickref/.
- MAKEINFO-PATH: path to the 'makeinfo' executable. Defaults to \"makeinfo\".
- CROSS-REFERENCE-VALIDATION: whether to validate cross references.
  Validation helps catch Declt bugs, but also prevents otherwise usable
  manuals from being generated. Defaults to T.
- ERROR-BEHAVIOR: behavior to adopt when errors are encountered.
  Possible values are :log (the default) and :abort. When :log, just log the
  errors and continue processing. Otherwise, stop at the first error. Note
  also that when errors are logged (which is meant to be the non-interactive
  way of running Quickref), normal output from Declt and Makeinfo is discarded
  completely.
- CACHE-POLICY: compilation cache handling policy.
  Possible values are :global (the default) and :local.
  When :global, the usual ASDF cache is used, meaning that compilation caches
  are shared across Declt calls. Otherwise, every call to Declt uses a
  different compilation cache. Note that the caching policy will automatically
  be set to :local if a version 2 parallel build is requested, as this
  particular algorithm is incompatible with the global ASDF cache.
- EMPTY-CACHE: whether to empty the compilation cache before proceeding.
  This parameter is only considered for a local cache policy. The global ASDF
  cache is never emptied. Defaults to T.
- PARALLEL: whether to parallelize the build process.
  Possible values are NIL (the default), 1, 2, 3, and 4 (or T). Numerical
  values correspond to different parallel build algorithms (see the file
  parallel.lisp for details). The most general one is #4, which is also
  selected by T.
- DECLT-THREADS: number of threads to use for Declt (Texinfo generation) when
  using a parallel build algorithm. Defaults to 2.
- MAKEINFO-THREADS: number of threads to use for Makeinfo (HTML generation)
  when using a parallel build algorithm. Defaults to 2."
  (check-type libraries (member :all :installed))
  (check-type error-behavior (member :log :abort))
  (check-type cache-policy (member :global :local))
  (check-type parallel (member nil 1 2 3 4 t))
  (when (and parallel (not (find-class 'context-// nil)))
    (error "Parallel support is not available."))
  (when (and (eq parallel 2) (eq cache-policy :global))
    (cond (cache-policy-p
	   (error "Can't use ASDF global cache with parallel algorithm #2."))
	  (t
	   (warn "Switching cache policy to :local for parallel algorithm #2.")
	   (setq cache-policy :local))))
  (when (eq parallel t) (setq parallel 4))
  (setq context (make-context cohort output-directory
			      makeinfo-path cross-reference-validation
			      error-behavior
			      cache-policy empty-cache))
  (when parallel
    (change-class context 'context-//
      :declt-threads declt-threads
      :makeinfo-threads makeinfo-threads))
  (when update
    (ql:update-client :prompt nil)
    (ql:update-dist (quicklisp-distribution) :prompt nil))
  (setq libraries
	(case libraries
	  (:all (ql-dist:provided-releases (quicklisp-distribution)))
	  (:installed (ql-dist:installed-releases (quicklisp-distribution)))))
  (when (and (eq (cache-policy context) :local) (empty-cache context))
    (renew-directories (cache-directory context)))
  (renew-directories (cohort-directory context)
		     (db-directory context)
		     (declt-logs-directory context)
		     (makeinfo-logs-directory context)
		     (texinfo-directory context)
		     (html-directory context))
  (ensure-directories-exist (cohort-db-directory context))
  (ensure-directories-exist (cohort-stats-directory context))
  ;; #### NOTE: foreign manuals are always built sequentially, which is
  ;; probably not such a big deal as they're unlikely to become numerous (even
  ;; if we make them customizable in the future). The reason has to do with
  ;; dependencies computation, which is required in some of the parallel
  ;; algorithms. Quicklisp provides that is some sort of a static database,
  ;; which allows us to read that without loading the systems. For foreign
  ;; libraries, we would have to ASDF:FIND-SYSTEM on them, which we want to
  ;; avoid in the main thread because it's somewhat fragile.
  (format t "~D foreign manual~:P to build.~%"
    (length *foreign-system-names*))
  (build-manuals *foreign-system-names* context nil)
  (format t "~D Quicklisp manual~:P to build.~%" (length libraries))
  (build-manuals libraries context parallel)
  (build-index-files context)
  (copy-style-sheets context)
  (build-cohort context)
  (values))

;; quickref.lisp ends here
