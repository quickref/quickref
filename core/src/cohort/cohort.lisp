;;; cohort.lisp --- Cohort entry point

;; Copyright (C) 2024 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)

(defun build-cohort
    (context
     &aux (db (when (cohort context)
		(mapcar #'uiop:safe-read-file-form
		  (directory (merge-pathnames "*.ct"
			       (truename (cohort-db-directory context))))))))
  "Build the Quickref cohort in CONTEXT."
  (when db
    (generate-cohort-statistics context db)
    (generate-cohort-plots context)
    (ensure-directories-exist (html-cohort-directory context))
    (uiop:run-program
	(list "tar" "-z" "-c"
	      "-C" (namestring (truename (cohort-directory context)))
	      "-f" (namestring
		    (merge-pathnames
			(make-pathname :name "cohort" :type "tgz")
		      (truename (html-cohort-directory context))))
	      "db"))
    (dolist (svg-file (directory
		       (merge-pathnames "*.svg"
			 (truename (cohort-stats-directory context)))))
      (let ((file-name (enough-namestring
			svg-file
			(truename (cohort-stats-directory context)))))
	(uiop:copy-file
	 svg-file
	 (merge-pathnames file-name (html-cohort-directory context)))))
    (let ((time (net.didierverna.declt::current-time-string))
	  (definitions 0)
	  (symbols 0))
      (mapc (lambda (library)
	      (mapc (lambda (item)
		      (when (symbol-definition-p item)
			(incf definitions)
			(when (unique-p item)
			  (incf symbols))))
		(cddr library)))
	db)
      (with-open-file (*standard-output*
		       (merge-pathnames
			   (make-pathname :name "index" :type "html")
			 (html-cohort-directory context))
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create
		       :external-format :utf-8)
	(format t (file-contents (merge-pathnames #p"cohort.html"
						  *templates-directory*))
	  (ql-dist:version (quicklisp-distribution))
	  definitions
	  symbols
	  time
	  (version :long)
	  (net.didierverna.declt:version :long))))))

;; cohort.lisp ends here
