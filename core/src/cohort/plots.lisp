;;; plots.lisp --- Cohort plots

;; Copyright (C) 2024 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


(defun generate-cohort-plots (context)
  "Generate the Quickref cohort plots in CONTEXT."
  (format t "Generating cohort plots...~%")
  (uiop:with-current-directory ((cohort-stats-directory context))
    (dolist (gp-file (directory (merge-pathnames "*.gp"
				  (truename *gp-directory*))))
      (let ((file-name (enough-namestring gp-file *gp-directory*)))
	(uiop:copy-file
	 gp-file
	 (merge-pathnames file-name (cohort-stats-directory context)))
	(uiop:run-program (list "gnuplot" (file-namestring gp-file)))))))

;; plots.lisp ends here
