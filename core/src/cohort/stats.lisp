;;; stats.lisp --- Cohort statistics

;; Copyright (C) 2024 EPITA Research and Development Laboratory

;; Author: Didier Verna
;; Maintainers: Didier Verna
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(in-package :fr.epita.lrde.quickref)


;; ==========================================================================
;; Utilities
;; ==========================================================================

(defun symbol-definition-p (item)
  (not (member (car item)
	       '(:contacts
		 :system-file :lisp-file :c-file :java-file
		 :html-file :doc-file :static-file
		 :source-file :file
		 :system :module
		 :package))))

(defun unique-p (item)
  (not (member (car item) '(:reader-method :writer-method :method))))

(defun unique-symbol-definition-p (item)
  (and (symbol-definition-p item) (unique-p item)))

(defun name (item &aux (name (second item)))
  (cond ((< (length name) 7)
	 name)
	((string= "(SETF " (subseq name 0 6))
	 (subseq name 6 (1- (length name))))
	(t
	 name)))



;; ==========================================================================
;; Symbol Statistics
;; ==========================================================================

(defun generate-symbol-statistics (db directory)
  (format t "  Symbol statistics...~%")
  (let ((lengths (make-array 64 :initial-element 0 :adjustable t))
	(cardinalities (make-array 8 :initial-element 0 :adjustable t))
	(sublengths (make-array 16 :initial-element 0 :adjustable t))
	(max-length '(0 nil nil))
	(max-cardinality '(0 nil nil))
	(max-sublength '(0 nil nil)))
    (mapc
	(lambda (library)
	  (mapc
	      (lambda (item &aux (name (name item)))
		(when (unique-symbol-definition-p item)
		  (macrolet ((update (value max array)
			       `(progn
				  (when (>= ,value (car ,max))
				    (setq ,max
					  (list ,value name (car library))))
				  (when (>= ,value
					    (car (array-dimensions ,array)))
				    (adjust-array ,array (* 2 ,value)
						  :initial-element 0))
				  (incf (aref ,array ,value)))))
		    (let ((length (length name)))
		      (update length max-length lengths))
		    (let* ((subnames (cl-ppcre:split "-" name))
			   (cardinality (length subnames)))
		      (update cardinality max-cardinality cardinalities)
		      (mapc (lambda (subname)
			      (let ((sublength (length subname)))
				(update sublength max-sublength sublengths)))
			subnames)))))
	    (cddr library)))
      db)
    (format t "  - Max length: ~A (~A), from ~A.~%"
      (second max-length) (first max-length) (third max-length))
    (format t "  - Max cardinality: ~A (~A), from ~A.~%"
      (second max-cardinality)
      (first max-cardinality)
      (third max-cardinality))
    (format t "  - Max sublength: ~A (~A), from ~A.~%"
      (second max-sublength) (first max-sublength) (third max-sublength))
    (flet ((export-symbol-stats (name array max)
	     (with-open-file
		 (stream (merge-pathnames
			     (make-pathname :name (concatenate 'string
						    "symbol-" name)
					    :type "dat")
			   directory)
		  :direction :output
		  :if-exists :supersede
		  :if-does-not-exist :create
		  :external-format :utf-8)
	       (dotimes (i (1+ (car max)))
		 (prin1 (aref array i) stream)
		 (terpri stream)))))
      (export-symbol-stats "lengths" lengths max-length)
      (export-symbol-stats "cardinalities" cardinalities max-cardinality)
      (export-symbol-stats "sublengths" sublengths max-sublength))))



;; ==========================================================================
;; Documentation Statistics
;; ==========================================================================

(defun generate-documentation-statistics (db directory)
  (format t "  Documentation statistics...~%")
  (let ((lengths (make-array 64 :initial-element 0 :adjustable t))
	(max-length '(0 nil nil))
	(%s (make-hash-table)))
    (mapc
	(lambda (library)
	  (mapc
	      (lambda (item &aux (name (name item)))
		(when (symbol-definition-p item)
		  (let ((length (cadr (member :docstring item))))
		    (macrolet ((update (value max array)
				 `(progn
				    (when (>= ,value (car ,max))
				      (setq ,max
					    (list ,value name (car library))))
				    (when (>= ,value
					      (car (array-dimensions ,array)))
				      (adjust-array ,array (* 2 ,value)
						    :initial-element 0))
				    (incf (aref ,array ,value)))))
		      (update length max-length lengths))
		    (flet ((update (kind &aux (current (gethash kind %s))
					      (doc (if (zerop length) 0 1)))
			     (cond (current
				    (rplaca current (+ (car current) doc))
				    (incf (cdr current)))
				   (t
				    (setf (gethash kind %s) (cons doc 1))))))
		      (case (car item)
			((:symbol-macro
			  :typed-structure-slot
			  :typed-structure :clos-structure
			  :reader-method :writer-method
			  :ordinary-reader :ordinary-writer
			  :generic-reader :generic-writer
			  :Macro-alias :compiler-macro-alias :function-alias))
			;; #### FIXME: this is wrong because Declt doesn't
			;; make a difference between a CLOS slot for a
			;; structure or an actual class or condition. So we
			;; cannot know whether the slot is supposed to get a
			;; documentation or not.
			((:clos-slot)
			 (update :slot))
			((:short-expander :long-expander)
			 (update :setf-expander))
			((:short-combination :long-combination :combination)
			 (update :method-combination))
			(t
			 (update (car item))))))))
	    (cddr library)))
      db)
    (format t "  - Max length: ~A (~A), from ~A.~%"
      (second max-length) (first max-length) (third max-length))
    (flet ((export-documentation-stats (name array max &optional upper-bound)
	     (with-open-file
		 (stream (merge-pathnames
			     (make-pathname :name (concatenate 'string
						    "docstring-" name)
					    :type "dat")
			   directory)
		  :direction :output
		  :if-exists :supersede
		  :if-does-not-exist :create
		  :external-format :utf-8)
	       (dotimes (i (1+ (min (car max) (or upper-bound (car max)))))
		 (prin1 (aref array i) stream)
		 (terpri stream)))))
      (export-documentation-stats "lengths" lengths max-length))
    (with-open-file
	(stream (merge-pathnames
		    (make-pathname :name "docstring-percentages" :type "dat")
		  directory)
	 :direction :output
	 :if-exists :supersede
	 :if-does-not-exist :create
	 :external-format :utf-8)
      (dolist (kind '(:constant :special :ordinary-function :macro
		      :generic-function :method :method-combination
		      :class :slot :condition
		      :setf-expander :compiler-macro :type))
	(let* ((stat (gethash kind %s))
	       (% (if (or (not stat) (zerop (cdr stat)))
		    0
		    (* (/ (car stat) (cdr stat)) 100.0))))
	  (format stream "\"~A\" ~A~%"
	    (nsubstitute #\Space #\- (string-capitalize kind))
	    %))))))



;; ==========================================================================
;; Documentation Statistics
;; ==========================================================================

(defun generate-definition-statistics (db directory)
  (format t "  Definition statistics...~%")
  (let ((numbers (make-hash-table))
	(avgs (make-hash-table)))
    (mapc
	(lambda (library)
	  (mapc
	      (lambda (item)
		(when (symbol-definition-p item)
		  (let ((kind (car item)))
		    (case kind
		      ((:clos-slot :typed-structure-slot :combination
			:reader-method :writer-method :method
			:ordinary-reader :ordinary-writer
			:generic-reader :generic-writer
			:macro-alias :compiler-macro-alias :function-alias))
		      (t
		       (setf (gethash kind numbers)
			     (1+ (or (gethash kind numbers) 0)))))
		    (flet ((update (kind &aux (current (gethash kind avgs)))
			     (cond (current
				    (incf (first current)
					  (cadr (member :direct-slots item)))
				    (incf (second current)
					  (cadr (member :direct-methods item)))
				    (incf (third current)
					  (cadr (member :direct-superclasses
							item)))
				    (incf (fourth current)
					  (cadr (member :direct-subclasses
							item)))
				    (incf (fifth current)))
				   (t
				    (setf (gethash kind avgs)
					  (list
					   (cadr (member :direct-slots item))
					   (cadr (member :direct-methods
							 item))
					   (cadr (member :direct-superclasses
							 item))
					   (cadr (member :direct-subclasses
							 item))
					   1))))))
		      (case kind
			((:clos-structure) (update :structure))
			((:class :condition) (update kind)))))))
	    (cddr library)))
      db)
    (with-open-file
	(stream (merge-pathnames
		 (make-pathname :name "definitions" :type "dat")
		 directory)
	 :direction :output
	 :if-exists :supersede
	 :if-does-not-exist :create
	 :external-format :utf-8)
      (dolist (kind '(:constant :special :ordinary-function :macro
		      :generic-function :short-combination :long-combination
		      :class :condition :typed-structure :clos-structure
		      :short-expander :long-expander
		      :symbol-macro :compiler-macro :type))
	(format stream "\"~A\" ~A~%"
	  (nsubstitute #\Space #\- (string-capitalize kind))
	  (or (gethash kind numbers) 0))))
    (with-open-file
	(stream (merge-pathnames
		 (make-pathname :name "classoids" :type "dat")
		 directory)
	 :direction :output
	 :if-exists :supersede
	 :if-does-not-exist :create
	 :external-format :utf-8)
      (format stream "\"Item\" \"Structure\" \"Class\" \"Condition\"~%")
      (loop :for item :in '(:slots :methods :parents :children)
	    :for i :from 0
	    :do (format stream "\"~A\"~{ ~A~}~%"
		  (string-capitalize item)
		  (mapcar (lambda (kind)
			    (let ((entry (gethash kind avgs)))
			      (if entry
				(float (/ (nth i entry) (fifth entry)))
				0.0)))
		    '(:structure :class :condition)))))))



;; ==========================================================================
;; Entry Point
;; ==========================================================================

(defun generate-cohort-statistics
    (context db &aux (stats-directory (cohort-stats-directory context)))
  "Generate Quickref cohort statistics in CONTEXT."
  (format t "Generating cohort statistics...~%")
  (generate-symbol-statistics db stats-directory)
  (generate-documentation-statistics db stats-directory)
  (generate-definition-statistics db stats-directory)
  (values))

;; stats.lisp ends here
