;;; fr.epita.lrde.quickref.core.asd --- Quickref core ASDF system definition

;; Copyright (C) 2018, 2019, 2022 EPITA Research and Development Laboratory

;; Author: Antoine Martin
;; Maintainers: Didier Verna and Antoine Martin
;; Contact: Quickref Maintainers <quickref-devel@common-lisp.net>

;; This file is part of Quickref.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Code:

(asdf:load-system :fr.epita.lrde.quickref.setup)

;; #### PORTME.
(asdf:defsystem :fr.epita.lrde.quickref.core
  :long-name "Quick Lisp Reference Manuals, core library"
  :description "Quickref's core functionality"
  :long-description "\
The Quickref core library provides the main functionality of Quickref. For a
more complete description of Quickref, see the `fr.epita.lrde.quickref'
system."
  :author "Antoine Martin"
  :maintainer ("Didier Verna" "Antoine Martin")
  :mailto "quickref-devel@common-lisp.net"
  :homepage
  "http://www.lrde.epita.fr/~didier/software/lisp/typesetting.php#quickref"
  :source-control "https://gitlab.common-lisp.net/quickref/quickref"
  :license "BSD"
  :version #.(fr.epita.lrde.quickref.setup:version :short)
  :if-feature :sbcl
  :defsystem-depends-on (:fr.epita.lrde.quickref.setup/parallel)
  :depends-on (:quicklisp :cl-ppcre :3bmd :net.didierverna.declt
	       :fr.epita.lrde.quickref.setup :fr.epita.lrde.quickref.cmark)
  :serial t
  :components ((:file "package")
	       (:module "src"
		:components ((:module "common"
			      :components ((:file "util")
					   (:file "context")
					   (:file "declt"
					    :depends-on ("util" "context"))
					   (:file "makeinfo"
					    :depends-on ("util" "context"))
					   (:file "readme"
					    :depends-on ("util"))
					   (:file "build")))
			     (:file "sequential" :depends-on ("common"))
			     (:file "parallel" :depends-on ("common")
			      :if-feature
			      (:and :sbcl :sb-thread
				    :fr.epita.lrde.quickref.parallel))
			     (:file "index" :depends-on ("common"))
			     (:module "cohort"
			      :depends-on ("common")
			      :components ((:file "stats")
					   (:file "plots"
					    :depends-on ("stats"))
					   (:file "cohort"
					    :depends-on ("plots"))))
			     (:file "quickref"
			      :depends-on ("common"
					   "sequential"
					   (:feature (:and :sbcl :sb-thread
							   :fr.epita.lrde.quickref.parallel)
						     "parallel")
					   "index"
					   "cohort"))))))

;;; fr.epita.lrde.quickref.core.asd ends here
